function save_options() {
    var userList = document.getElementById('userList').value;
    chrome.storage.sync.set({
        userList: userList,
    }, function () {
        // Update status to let user know options were saved.
        var status = document.getElementById('status');
        status.textContent = 'Options saved.';
        setTimeout(function () {
            status.textContent = '';
        }, 750);
    });
}

// Restores select box and checkbox state using the preferences
// stored in chrome.storage.

var defaultUsers = {
    bau: {
        cz: {
            users: [
                {
                    username: 'testUser',
                    password: 'password',
                    description: 'User description'
                }
            ],
        },
        ro: {
            users: [],
        }
    },
    fusion: {
        cz: {
            users: [],
        },
        ro: {
            users: [],
        }
    }
};

function restore_options() {
    // Use default value color = 'red' and likesColor = true.
    chrome.storage.sync.get({
        userList: '',
    }, function (items) {
        console.log('items', items);
        items.userList = items.userList || JSON.stringify(defaultUsers, null, 2);
        document.getElementById('userList').value = items.userList;
    });
}

document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click',
    save_options);
