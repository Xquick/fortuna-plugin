document.getElementById('user_li').addEventListener('click', function () {
    openUserExtension();
});

function openUserExtension() {
    chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, {message: "userList"});
        window.close();
    });
}
