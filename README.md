# Chrome Extension for Fortuna apps
Collection of following  Fortuna tools 
 * User login helper

## Installation
 1. `npm ci`
 2. `npm run build`
 3. In Chrome, go to url `chrome://extensions`
 4. Check you have `Developer mode` switcher turned on
 5. Click on `Load unpacked` button and navigate into `build` folder and Select it
 6. There might be some ERROR (depending on current state of the plugin source code :) ), dont mind it
 7. Now you have new Fortuna icon in the Chrome toolbar
 
## Usage
###User list
(Currently usable only with Live3 app)

If you click on yellow Fortuna icon in chrome toolbar, there is menu containing `User list` option. 
Click it and popup with users will append into the page.

List of users is editable from by clicking plugin icon with right mouse button and selecting `Options`. 
There is JSON textarea, where you can edit your users.
