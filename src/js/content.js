var defaultUsers = {
    bau: {
        cz: {
            users: [
                {
                    username: 'testUser',
                    password: 'password',
                    description: 'User description'
                }
            ],
        },
        ro: {
            users: [],
        }
    },
    fusion: {
        cz: {
            users: [],
        },
        ro: {
            users: [],
        }
    }
};

chrome.storage.sync.get({
    userList: '',
}, function (items) {
    items.userList = items.userList || JSON.stringify(defaultUsers);
    var userList = JSON.parse(items.userList);

    if (typeof user === 'undefined') {
        var user = '';
    }

    if (typeof user === 'undefined') {
        var password = '';
    }

    function openLoginForm() {
        document.querySelectorAll('div.login_form__button')[0]
            .click();
    }

    function createElementFromHTML(htmlString) {
        var div = document.createElement('div');
        div.innerHTML = htmlString.trim();

        // Change this to div.childNodes to support multiple top-level nodes
        return div.firstChild;
    }

    function showUserTable(manual = true) {
        if (!manual) {
            var wasOpenUsersPopup = localStorage.getItem('fortuna-plugin-users');
            if (!wasOpenUsersPopup) return;
        }

        localStorage.setItem('fortuna-plugin-users', true);

        var users = document.getElementById('users');

        if (users && manual) {
            console.log('removing');
            document.getElementById('users').remove();
        }

        var userHTMLTable = `
        <div id="users">
            <i class="icon--cancel"
                onclick="
                document.getElementById('users').remove();
                localStorage.removeItem('fortuna-plugin-users');"></i>
                <div class="table-container">
            <table>
              <tr>`;
        Object.entries(userList).forEach((envEntry) => {
            const environmentName = envEntry[0];
            const countryList = envEntry[1];
            userHTMLTable += `<tr class="env__row"><td class="env__cell"><strong>${environmentName}</strong></td><td></td></tr>`;

            Object.entries(countryList).forEach((countryEntry) => {
                const countryName = countryEntry[0];
                const userArray = countryEntry[1].users;
                if (userArray) {
                    userHTMLTable += `<tr><td class="country__cell"><strong>${countryName}</strong></td><td></td></tr>`;

                    userArray.forEach((user) => {
                        if (user) {
                            userHTMLTable += `<tr class="user__row"><td class="user__cell"
                                                onclick="
                                                   document.querySelectorAll('div.login_form__button')[0]
                                                        .click();
                                                    document.querySelectorAll('input[name=username]')[0].value = '${user.username}';
                                                    document.querySelector('input[name=username]').dispatchEvent(new Event('input', { 'bubbles': true }))
                                                    document.querySelectorAll('input[name=password]')[0].value = '${user.password}';
                                                    document.querySelector('input[name=password]').dispatchEvent(new Event('input', { 'bubbles': true }))
                                            ">${user.username}</td><td>${user.description}</td></tr>`;
                        }
                    });
                }
            })
        });

        userHTMLTable += `
      </tr>
      </table>
      </div>
      <button class="action__logout" onclick="document.querySelectorAll('.user_panel__logout>div')[0].click()">Logout</button>
      <button class="action__login" onclick="document.querySelectorAll('.login_form__submit_button>div')[0].click()">Login</button>
    </div>`;

        document.body.appendChild(createElementFromHTML(userHTMLTable));

    }

    var fillLoginInfoDOM = (user, password) => {
        document.querySelectorAll('input[name=username]')[0].value = user;
        document.querySelectorAll('input[name=password]')[0].value = password;
    };

    chrome.runtime.onMessage.addListener(
        function (request, sender) {
            if (request.message === 'userList') {
                showUserTable();
            }
        });


    showUserTable(false);

    document.head.appendChild(createElementFromHTML(`<script></script>`));
});
